const casesCtrl = {};

const Case = require('../models/Case');

casesCtrl.renderCaseForm =(req, res) => {
  res.render('cases/new-case');
};

casesCtrl.createNewCase = async (req, res) => {
  const {title, description, name, lastname, cel, tipoCase, ubicacion, correo, pruebaCaso} = req.body;
  const newCase = new Case({title, description, name, lastname, cel, tipoCase, ubicacion, correo, pruebaCaso});
  await newCase.save();
  res.redirect('/cases')
};

casesCtrl.renderCase = async (req, res) => {
  const cases = await Case.find().lean();
  res.render('cases/all-cases', { cases });
};

casesCtrl.renderEditForm = async (req, res) => {
  const caso = await Case.findById(req.params.id);
  console.log(caso)
  res.render('cases/edit-case', { caso });
};

casesCtrl.updateCase = (req, res) => {
  res.send('Update Case');
};

casesCtrl.deleteCase = async (req, res) => {
  await Case.findByIdAndDelete(req.params.id);
  res.redirect('/cases')
};

module.exports = casesCtrl;