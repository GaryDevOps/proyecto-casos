const { Router } = require('express');
const router = Router()

const { 
  renderCaseForm,
  createNewCase,
  renderCase,
  renderEditForm,
  updateCase,
  deleteCase
} = require('../controllers/cases.controller');

// New Case
router.get('/cases/add', renderCaseForm);

router.post('/cases/new-case', createNewCase);

// Get all Cases
router.get('/cases', renderCase);

// Edit Cases
router.get('/cases/edit/:id', renderEditForm);

router.put('/cases/edit/:id', updateCase);

// Delete Case
router.delete('/cases/delete/:id', deleteCase);

module.exports = router