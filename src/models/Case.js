const { Schema, model } = require('mongoose');

const CaseSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
      type: String,
      required: true
  },
  name: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  cel: {
    type: String,
    required: true
  },
  tipoCase: {
    type: String,
    required: true
  },
  ubicacion: {
    type: String,
    required: true
  },
  correo: {
    type: String,
    required: true
  },
  pruebaCaso: {
    type: String,
    required: true
  }
}, {
  timestamps: true
})

module.exports = model('Case', CaseSchema);