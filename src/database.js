const mongoose = require('mongoose');

const { NOTES_APP_MONGODB_HOST, NOTES_APP_MONGODB_DATABASEI } = process.env;
const MONGODB_URI = `mongodb://${NOTES_APP_MONGODB_HOST}/${NOTES_APP_MONGODB_DATABASEI}`;

mongoose.connect(MONGODB_URI, {
 
})
  .then(db => console.log('Database is connected'))
  .catch(err => console.log(err));